# Please-Make

is a Command Line Interface (CLI) tool designed to help you create files. This tool is built with Node.js and uses the Commander package to create the CLI.

## Installation
To install please-make, run the following command:

```
npm install -g please-make
```

## Usage
please-make provides three commands to create files: model,controller, and route.

### model
Use this command to create a model. To use this command, run:
```
please-make model <model-name>
```

<model-name> is the name of the model you want to create, such as "users" or "user_book".

### controller
Use this command to create a controller. To use this command, run:
```
please-make controller <controller-name> [options]
```

<controller-name> is the name of the controller you want to create, such as "users" or "user_book".

The following options are available:

-m, --model <model-name>: This option lets you specify the name of the model associated with the controller.
-x, --create: This option lets you create a new model if one doesn't already exist.

### route
Use this command to create a router. To use this command, run:
```
please-make route <router-name> [options]
```

<router-name> is the name of the router you want to create, such as "users" or "user_book". The following options are available:

-c, --controller <controller-name>: This option lets you specify the name of the controller associated with the router.
-x, --create: This option lets you create a new controller and model if they don't already exist.

### Version
To check the version of please-make you have installed, run:

```
please-make --version
```

License
please-make is licensed under the MIT license. See LICENSE for more details.
