#!/usr/bin/env node

const {Command} = require('commander')
const makeModel = require('./makeModel')
const makeController = require('./makeController');
const makeRouter = require('./makeRouter');
const makeMigration = require('./makeMigration');

const program = new Command();


program.name('ccci-cli')
    .description('CLI to create file')
    .version('1.0.0')


program.command('model')
    .description('Create a model')
    .argument('<string>', 'model name, eg. [users, user_book]')
    .action((str, options) => {
        makeModel(str, options)
    })

program.command('controller')
    .description('Create a controller')
    .argument('<string>', 'controller name, eg. [users, user_book]')
    .option('-m, --model <char>', 'model name eg. [users, user_book]')
    .option('-x, --create', 'create model')
    .action((str, options) => {
        makeController(str, options)
    })

program.command('route')
    .description('Create a controller')
    .argument('<string>', 'controller name, eg. [users, user_book]')
    .option('-c, --controller <char>', 'controller name eg. [users, user_book]')
    .option('-x, --create', 'create controller and model and migration')
    .action((str, options) => {
        makeRouter(str, options)
    })

program.command('migration')
    .description('Create a migration')
    .argument('<string>', 'migration name, eg. [users, user_book]')
    .action((str, options) => {
        makeMigration(str, options)
    })


program.parse(process.argv)
