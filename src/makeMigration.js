
const { exec } = require('child_process');
const { toPascalCase } = require('js-string-helper')

const loading = require('loading-cli')
module.exports = (name, option) => {
    name = toPascalCase(name)
    const loader = loading(`creating migration script: ${name}.`).start()
    exec(`npx sequelize-cli migration:create --name ${name}`, (error, stdout, stderr) => {
        if (error) loader.fail(error.message)

        if (stdout) loader.succeed('Migration files has been created: ' + stdout)

        if (stderr) loader.fail(stderr)

        loader.stop()
    } )
}
