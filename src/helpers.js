const { toPascalCase, toSnakeCase } = require("js-string-helper");

const fs = require("fs");
const path = require("path");

exports.getClassName = (str) => {
  return toPascalCase(str.replace(/[-_ ]/, " "));
};

exports.getFileName = (str, suffix = "") => {
  return toSnakeCase(str).split(/[-_]/).join(".") + suffix;
};

exports.getRouterFileName = (str, suffix = "") => {
  return toSnakeCase(str).split("_").join("-") + suffix;
};

exports.getModelName = (str) => {
  return toPascalCase(str.replace(/[-_ ]/, " "));
};

function directoryExists(path) {
  try {
    return fs.statSync(path).isDirectory();
  } catch (error) {
    return false;
  }
}

exports.getUserPath = (fileName, dir) => {
  let pathDir = path.join(process.cwd(), dir);
  if (directoryExists(pathDir)) {
    return path.join(pathDir, fileName);
  }

  throw new Error(`directory ${dir} not found.`);
};
