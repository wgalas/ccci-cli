const { getClassName, getFileName, getModelName, getUserPath } = require('./helpers')
const createFileFromTemplate = require('./createFileFromTemplate')

module.exports = (name, options) => {
    let className = getClassName(name)
    let fileName = getFileName(name, '.model.js')
    let modelName = getModelName(name);
    let filePath = getUserPath(fileName, 'models');
    // TODO handle options
    createFileFromTemplate('model', filePath, {className, modelName})
}
