const { getClassName, getFileName, getRouterFileName, getModelName, getUserPath } = require('./helpers')
const createFileFromTemplate = require('./createFileFromTemplate')
const makeController = require('./makeController')
const makeModel = require('./makeModel')
const makeMigration = require('./makeMigration')

module.exports = (name, options) => {
    let className = getClassName(name)
    let fileName = getRouterFileName(name, '.js')
    let controllerFileName = getFileName(name)
    let filePath = getUserPath(fileName, 'routes/v1.0')


    if (options['create']) {
        makeController(name, {})
        makeModel(name)
        makeMigration(`create ${name} table`);
    }

    // TODO handle options
    createFileFromTemplate('router', filePath, {className, controllerFileName})
}
