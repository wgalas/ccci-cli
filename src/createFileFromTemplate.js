const fs = require('fs')
const path = require('path')
const loading = require('loading-cli')


module.exports = (template, file, load) => {
    const loader = loading(`creating ${file}.`).start()
    try {
        let templateDir = path.join(__dirname, '/../', 'templates');
        let templatePath = path.join(templateDir, template + '.txt')
        fs.readFile(templatePath, 'utf-8', (error, data) => {
            if (error) throw error;

            let content = data;
            for (prop in load) {
                content = content.replaceAll(`@@${prop}@@`, load[prop]);
            }

            fs.writeFile(file, content, 'utf-8', (error) => {
                if (error) throw error;

                loader.succeed(`file: ${file} has been created.`)
            })
        })
    } catch (error) {
        loader.fail(error.message)
    }
}
