const createFileFromTemplate = require('./createFileFromTemplate');
const {getFileName, getModelName, getUserPath, getClassName } = require('./helpers')
const makeModel = require('./makeModel')
module.exports = (name, options) => {
    let className = getClassName(name);
    let modelName = getModelName(name)
    let fileName = getFileName(name, '.controller.js')
    let filePath = getUserPath(fileName, 'controllers')

    if (options['model']) {
        modelName = options['model']
    }

    if (options['create']) {
        makeModel(modelName, {})
    }

    createFileFromTemplate('controller', filePath, {modelName, className})
}
