import BaseController from "../utils/BaseController.js";

import Model from "../models/index.js";
const { @@modelName@@ } = Model.getDb();

export default class @@className@@Controller extends BaseController {
  constructor() {
    super(@@modelName@@);
  }
}
